# Developing a Single Page App with Flask and Vue.js

Follow this course on https://testdriven.io/blog/developing-a-single-page-app-with-flask-and-vuejs/ if you want to be able to:

* Explain what Flask is
* Explain what Vue is and how it compares to other UI libraries and front-end frameworks like React and Angular
* Scaffold a Vue project using the Vue CLI
* Create and render Vue components in the browser
* Create a Single Page Application (SPA) with Vue components
* Connect a Vue application to a Flask back-end
* Develop a RESTful API with Flask
* Style Vue Components with Bootstrap
* Use the Vue Router to create routes and render components